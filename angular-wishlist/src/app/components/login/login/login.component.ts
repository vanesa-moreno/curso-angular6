import { useAnimation } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  messageError: string;

  constructor(public authService: AuthService) { 
    this.messageError = "";
  }

  ngOnInit(): void {
  }

  login(username: string, password: string): boolean{
    this.messageError = "";
    if(!this.authService.login(username, password)){
      this.messageError = "Login incorrecto";
      setTimeout(function(){
        this.messageError = "";
      }.bind(this), 2500);
      return false;
    }
  }

  logout(){
    this.authService.logout();
    return false;
  }

}
