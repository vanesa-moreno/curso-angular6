import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinoApiClient } from 'src/app/models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/models/destinos-viajes-state.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinoApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  destinos: DestinoViaje[];
  all;

  constructor(public destinosApiClient : DestinoApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(d => {
      //const d = data;
      if (d != null){
        this.updates.push('se ha elegido a: ' + d.nombre);
      }
    });
    this.all= store.select(state => state.destinos.items).subscribe(items =>this.all = items);
  }

  ngOnInit(): void {
  }

	agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));
  }
  
  elegido(d: DestinoViaje){
    this.destinosApiClient.elegir(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
